import discord

from operator import itemgetter


# Mock for local testing
class MockDiscordUser(discord.User):
    def __init__(self, name):
        self.name = name


# ------------------------------
# Player Roles
class UserRole(object):

    def __init__(self, discord_user: discord.User, **kwargs):
        super().__init__(**kwargs)

        self.discord_user = discord_user

        self.is_alive         = True
        self.marked_death     = False
        self.marked_protected = False

    def get_name(self):
        return self.discord_user.name


class Town(UserRole):

    order_rank_night = 1000

    def night_interaction(self):
        pass


class Mafia(UserRole):

    order_rank_night = 1

    @classmethod
    def kill(cls, selected_user_role):
        selected_user_role.marked_death = True

        return f'You have put a hit on {selected_user_role.get_name()}.'

    def night_interaction(self):
        return {
            'message'  : 'who would you like to have killed?',
            'fn_action': Mafia.kill
        }


class Investigator(Town):

    order_rank_night = 2

    @classmethod
    def investigate(cls, selected_user_role):
        return 'You discovered that {} {}.'.format(
            selected_user_role.get_name(),
            (
                '*is* Mafia'
                if type(selected_user_role) is Mafia
                else '*not* Mafia'
            )
        )

    def night_interaction(self):
        return {
            'message'  : 'who would you like to investigate?',
            'fn_action': Investigator.investigate
        }


class Doctor(Town):

    order_rank_night = 3

    @classmethod
    def doctor_save(cls, selected_user_role):
        selected_user_role.marked_protected = True

        return f'You have agreed to be on-call to save {selected_user_role.get_name()}'

    def night_interaction(self):
        return {
            'message'  : 'who would you like to save?',
            'fn_action': Doctor.doctor_save
        }


if __name__ == '__main__':
    print(discord.__version__)

    test_mafia  = Mafia(discord_user=MockDiscordUser('Octalene'))
    test_doctor = Doctor(discord_user=MockDiscordUser('Zoycir'))
    test_cop    = Investigator(discord_user=MockDiscordUser('Therusski88'))
    test_town   = Town(discord_user=MockDiscordUser('V0id'))

    # pretend night round
    special_roles = (test_mafia, test_cop, test_doctor)
    extract_interaction = itemgetter('message', 'fn_action')

    for role_ndx, special_role in enumerate(special_roles):
        msg_text, fn_role_action = extract_interaction(special_role.night_interaction())

        print(f'Night falls and the townspeople sleep. {msg_text}')

        print(fn_role_action(special_roles[role_ndx - 1]))

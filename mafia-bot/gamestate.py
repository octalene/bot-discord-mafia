import itertools


# ------------------------------
# High-level game state
class Phase(object):

    @classmethod
    def start(cls, user_roles):
        return cls(user_roles)

    def __init__(self, **kwargs):
        self.completed = False

    def end(self):
        self.completed = True


class DayPhase(Phase):

    def __init__(self, user_roles, **kwargs):
        super().__init__(**kwargs)

        self.user_roles = user_roles


class NightPhase(Phase):

    def __init__(self, user_roles, **kwargs):
        super().__init__(**kwargs)

        self.user_roles = itertools.chain(
            user_roles
        )
